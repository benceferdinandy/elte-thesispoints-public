tex = lualatex
bib = biber
date = $(shell date --iso=date)
texfile_en = main_en
bibfile_en = refs_en.bib
texfile_hu = main_hu
bibfile_hu = refs_hu.bib
#~ bibfile_en = /home/fbence/Documents/bence_articles.bib
final_en = Ferdinandy_thesis_points
final_hu = Ferdinandy_tézispontok
#~ current_dir := $(shell ${PWD##*/})

all: $(texfile_en).bbl $(texfile_en).pdf $(texfile_hu).bbl $(texfile_hu).pdf

refs:
	$(tex) -interaction=nonstopmode $(texfile_en).tex
	$(bib) $(texfile_en)
	$(tex) -interaction=nonstopmode $(texfile_hu).tex
	$(bib) $(texfile_hu)

$(texfile_en).pdf: $(texfile_en).bbl $(texfile_en).tex titlepage_en.tex fbence_thesispoints.cls 
	$(tex)  --synctex=1 $(texfile_en).tex 
	mkdir -p versions
	cp $(texfile_en).pdf versions/$(final_en)_$(date).pdf
	cp $(texfile_en).pdf $(final_en).pdf


$(texfile_en).bbl: $(bibfile_en)
	$(tex)  --synctex=1 $(texfile_en).tex
	$(bib) $(texfile_en)
	
$(texfile_hu).pdf: $(texfile_hu).bbl $(texfile_hu).tex titlepage_hu.tex fbence_thesispoints.cls 
	$(tex)  --synctex=1 $(texfile_hu).tex 
	mkdir -p versions
	cp $(texfile_hu).pdf versions/$(final_hu)_$(date).pdf
	cp $(texfile_hu).pdf $(final_hu).pdf


$(texfile_hu).bbl: $(bibfile_hu)
	$(tex)  --synctex=1 $(texfile_hu).tex
	$(bib) $(texfile_hu)

	
clear:
	rm *.aux *.xml *.log *.bbl *.bcf *.blg *.out *.snm *.toc *.nav


