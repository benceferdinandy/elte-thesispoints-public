# PhD Thesis points template for ELTE

use `latexmk -lualatex main_en.tex -f` and `latexmk -lualatex main_hu.tex -f` to build
if something is not working `latexmk -c` will clean up all

See the dissertation for more info [here](https://bitbucket.org/benceferdinandy/dissertation-public/src/master/)


# refs_en.bib and refs_hu.bib

Difference is only `pubstate`. English is same as `selfref.bib` in the dissertation.

# magyar.lbx

Biblatex currently (2018 april) has no native hungarian support. We are working on it, so might not be needed later, check [here](https://github.com/plk/biblatex/issues/717)
