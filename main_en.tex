\documentclass[]{ELTE_thesispoints}
\setmainfont[]{Source Sans Pro}

\setmainlanguage{english}

%citations
\usepackage[defernumbers=true,sorting=none,backend=biber,maxnames=10,style=ieee]{biblatex}
\addbibresource{refs_hu.bib}

\renewcommand*{\mkbibnamegiven}[1]{%
  \ifitemannotation{corresponding}
    {\textbf{#1}}
    {#1}}
\renewcommand*{\mkbibnamefamily}[1]{%
  \ifitemannotation{corresponding}
    {\textbf{#1}}
    {#1}}

\DeclareBibliographyCategory{cited}
\AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}


\settoggle{bbx:url}{false}
\settoggle{bbx:doi}{false}
\settoggle{bbx:eprint}{false}
\settoggle{bbx:isbn}{false}
\AtEveryBibitem{ \clearlist{language}} 
\def\bibfont{\small}

%titlepage infos
\title{Realistic modelling of complex systems of biological agents: epidemiology of HIV on complex sexual networks and collective motion of hierarchical herds}
\author{Bence Ferdinandy}
\school{Doctoral School Of Physics}
\schoolhead{Prof. Dr. Tamás Tél}
\program{Statistical physics, biological physics and physics of quantum systems program}
\programhead{Prof. Dr. Jenő Kürti}
\unilogo{./fig/elte_cimer_color.pdf}
\unilogoscale{0.2}
\supervisor{Tamás Vicsek}
\supervisortitle{Professor of Physics}
\department{Department of Biological Physics}
\faculty{Faculty of Science}
\university{Eötvös Loránd University}
\subject{complex systems}

\date{\the\year}



\begin{document}


\input{titlepage_en}

%\listmytodos\newpage


\section{Motivation}

%\tdnote{similarities}{
%\begin{itemize}
%\item emergent phenomena in complex systems
%\item agent based modelling
%\item hierarchy: qualitatively different groups (men and women vs fsw;
%leader vs follower) (the degree is aprox. group size so they are not
%both hierarchical in that sense, although if lognormal counts as
%hiearchical, then the original systems both share this property as well)
%\item 
%\end{itemize}
%}
The study of complex systems -- comprised of many units, the interactions of which give rise to unique global phenomena -- has gained momentum with the rise of computational power and significantly increasing data availability on a variety of systems. The very nature of complex systems - i.e. that the interactions between units are usually rather simple, but the number of units make calculations very hard - fits well with computer simulation and thus computer simulations are one of the primary methods of their research. A straightforward methodology for such simulations is agent-based modelling. In such an approach each of the units are modelled separately as a unique agent, encompassing any relevant parameters pertaining to the units of the system, and subsequently, the interaction between the units are modelled as interactions between the agents.

The most profound examples of complex systems are produced by life. Biological systems of any scale, from simple cells to the biosphere are inherently driven to complexity by evolution and they also constitute the most important systems for us to understand. The modelling of biological systems in a realistic manner is very problematic compared with physical systems, since in biological systems the constituent units are usually themselves complex systems in their own right. Choosing the appropriate parameters to incorporate into a model to be able to compare it with reality, yet not to over fit the model is non-trivial. For example, the best current approach in modern computer games to maximize the feeling of reality is by modelling physical entities with the appropriate physical laws and modelling characters with recordings of live actors.


The present PhD dissertation is focused on the above problem of modelling biological systems in a realistic way, using agent-based modelling. In particular, I present the results of two studies I conducted with my co-authors, where we explored the modelling of two complex systems, in which there are not just quantitatively, but qualitatively different types of units.

In the first study the system we considered is the network of human (hetero)sexual contacts, where female sexual workers (FSWs) behave very differently from males and non-FSW females. In this system we examined the spreading of various strains of HIV and their competition. In the second study we investigated a two-level hierarchy of leaders and followers in the context of collective motion and group formation, which is used to model the collective motion of a wild horse herd. In both cases the models were built on data available in the literature or our own observations and the properties of the emergent behaviours of the models are compared to the processes  found in the real systems. Our results can be summarized in six theses as follows.


\newpage
\section{Theses}
\begin{enumerate}
\item {\bfseries We created an agent--based  framework for studying the
spread of sexually transmitted diseases in heterosexual communities.}

The framework consists of a temporal dynamic network model of sexual contacts, which statistically adheres to empirical data
available on the time-integrated degree distributions of human sexual contact networks. The model consists of three types of agents, namely female sex workers (FSWs), males, and non-FSW females. The time-integrated degree
distribution of males and non-FSW females are powerlaws with differing
exponents, while FSWs have a very high constant degree (derived from literature) and also behave differently, than the other two groups.~\autocite{Ferdinandy2015}

\item {\bfseries We created a model of HIV infection, which gives compatible timescales with HIV/AIDS epidemic history.}

Based on the available empirical data we created a simplified model of HIV infection, splitting the
infection into a more contagious acute period and a less contagious
chronic period. We tested the model with the above framework, recovering HIV/AIDS
epidemic timescales compatible with the known history of the HIV/AIDS
epidemic in the 20\textsuperscript{th} century.~\autocite{Ferdinandy2015}

\item {\bfseries When two strains of HIV are introduced after each other into the same population, the second strain takes a very long time to be dominant, even if it is much stronger than the first.}

Utilizing the above model and framework, we measured whether a
second strain of HIV could potentially invade an already stable
HIV/AIDS epidemic, considering various possibilities of interaction
between the two strains. We found that the second strain needs to be
considerably ($>25\%$) stronger to be able to outgrow the first strain
on the timescale of decades, which is consistent with the current
phylogeography of HIV outside Africa, i.e. that populations are
infected by a single dominant strain.~\autocite{Ferdinandy2015}

\item {\bfseries We created the first collective motion model of self-propelled
particles with two distinct type of agents, representing a two-level
animal society.}

A model originally proposed to describe
the movement of cells resulting in a smoothly varying coherent motion
was adapted to model two-level assemblies of self-propelled
particles. We adopted a terminology corresponding to large groups of some
mammals, where leaders and followers form a group called a harem, 
and leaders without followers may form so-called bachelor groups.~\autocite{ferdinandy2017horses}

\item {\bfseries We showed, that the above model is in qualitative agreement with the movement 
of a wild horse herd on an open plain.}

We compared the emergent behavior of the model with observations
of a Przewalski horse herd. We found that relatively small number of
parameters have to be tuned to produce behavior that is in qualitative agreement with the motion of a
horse herd moving on an open plain.  With the appropriate parameters the model will form groups of leaders and
followers and bachelor groups, with roughly adequate correspondence of dimension scales.
The model also shows ordered rotational movement which has also been
observed in some quadrupeds.~\autocite{ferdinandy2017horses}

\item {\bfseries We found indirect evidence that models based on spatial interactions are unable to recover
important statistical properties of social interactions within a co-moving herd.}

We found that our model - which is based on spatial interactions -,
produces a normal distribution of group sizes, in contrast with the
empirical lognormal group size distribution of the Przewalski horse herd. Although the model was successful 
in replicating the overall movements of the herd, this show the limits of such
an approach to modeling emergent properties of groups of social
animals.~\autocite{ferdinandy2017horses}
\end{enumerate}


\newpage
\nocite{Ferdinandy2012}
\nocite{abdai2017chasing}
\nocite{ferdinandy2017wolfs}
\nocite{abramsensdog2017}


\singlespacing

\printbibliography[title={Publications included in theses},category=cited]% default title for `article` class: "References"

\printbibliography[title={Publications not included in theses},notcategory=cited]

\end{document}
