%
% Created by Bence Ferdinandy (bence@ferdinandy.com)
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ELTE_thesispoints}[2018/04/27 ELTE thesispoints class]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions \relax
\LoadClass[a5paper]{scrartcl} %european article style

\RequirePackage{todonotesextra}

\RequirePackage[version=3]{mhchem} %chemistry
\RequirePackage{enumitem} %enumerate nicer

%design
\RequirePackage{fancyhdr}
\RequirePackage{geometry}

%math: the ams package provide characters and symbols
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage[tight,nice]{units} %for typesetting units of measurements in a nice way (also loads nicefrac)
\RequirePackage{esdiff} %derivatives and partial derivatives in an easy way

\RequirePackage{booktabs}
\RequirePackage{setspace}
\onehalfspacing


%nicer squareroot
\RequirePackage{letltxmacro}
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2}}


%figures
\RequirePackage[above,below]{placeins}
\RequirePackage{graphicx}
\RequirePackage{subcaption}


%fonts

\RequirePackage[no-math]{fontspec}


%language
\RequirePackage{polyglossia}
\RequirePackage{csquotes}



  


\sloppy %make latex prefer looser lines than overflows
%bookmarks and references
\RequirePackage[unicode,bookmarks=true,colorlinks=false,hidelinks]{hyperref}

\AtBeginDocument{
  \hypersetup{
    pdftitle = {\@title},
    pdfauthor = {\@author}
  }
}


\def\@school{}
\newcommand{\school}[1]{
  \def\@school{#1}
}

\def\@schoolhead{}
\newcommand{\schoolhead}[1]{
  \def\@schoolhead{#1}
}

\def\@program{}
\newcommand{\program}[1]{
  \def\@program{#1}
}

\def\@programhead{}
\newcommand{\programhead}[1]{
  \def\@programhead{#1}
}
\def\@supervisor{}
\newcommand{\supervisor}[1]{
  \def\@supervisor{#1}
}

\def\@supervisortitle{}
\newcommand{\supervisortitle}[1]{
  \def\@supervisortitle{#1}
}

\def\@department{}
\newcommand{\department}[1]{
  \def\@department{#1}
}

\def\@faculty{}
\newcommand{\faculty}[1]{
  \def\@faculty{#1}
}

\def\@university{}
\newcommand{\university}[1]{
  \def\@university{#1}
}

\def\@unilogo{}
\newcommand{\unilogo}[1]{
  \def\@unilogo{#1}
}

\def\@unilogoscale{}
\newcommand{\unilogoscale}[1]{
  \def\@unilogoscale{#1}
}
